// CRUD Operations Create Read Update Delete
// const mongodb = require('mongodb')
// const MongoClient = mongodb.MongoClient
// const ObjectID = mongodb.ObjectID

const { MongoClient, ObjectID } = require('mongodb')

const connectionURL = 'mongodb://127.0.0.1:27017'
const dataBaseName = 'task-manager'
const databaseName2 = 'task-collection'

MongoClient.connect(connectionURL, { useNewUrlParser: true }, (error, client) => {
    if (error) {
        return console.log("Unable to Connect...");
    }
    console.log("Connected ..")
    const db = client.db(dataBaseName)

    // db.collection('user').deleteMany({
    //     College: "SIT"
    // }, ).then((result) => console.log(result.deletedCount)).catch((err) => console.log(err))

    db.collection('user').deleteOne({
        name: "Aish"
    }).then((result) => console.log(result.deletedCount)).catch((err) => console.log(err))
})