const express = require('express')
const MyTasks = require('../models/task')
const auth = require('../middleware/auth')
const router = new express.Router();

router.post('/tasks', auth, async(req, res) => {
    // const task = new MyTasks(req.body)
    const task = new MyTasks({
        ...req.body,
        owner: req.user._id
    })
    try {
        await task.save()
        res.status(400).send(task)
    } catch (e) {
        res.status(500).send(e.message)
    }
})

//GET /tasks?isCompleted=true
//GET /tasks?limit=10&skip=0
//GET /tasks?sortBy=createdBy:desc
router.get('/tasks', auth, async(req, res) => {
    const match = {}
    const sort = {}

    if (req.query.isCompleted) {
        match.isCompleted = req.query.isCompleted === 'true'
    }

    if (req.query.sortBy) {
        const parts = req.query.sortBy.split(':')
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1
    }
    try {
        //const myTasks = await MyTasks.find({ owner: req.user._id })
        await req.user.populate({
                path: 'userTask',
                match,
                options: {
                    limit: parseInt(req.query.limit),
                    skip: parseInt(req.query.skip),
                    sort
                }
            }).execPopulate()
            //res.status(200).send(myTasks)
        res.send(req.user.userTask)
    } catch (e) {
        res.status(500).send(e.message)
    }
})

router.get('/tasks/:id', auth, async(req, res) => {
    const _id = req.params.id
    try {
        //const myTasks = await MyTasks.findById({ _id })
        const myTasks = await MyTasks.findOne({ _id, owner: req.user._id })
        if (!myTasks) {
            res.status(404).send("Task Not Found")
        }
        res.status(200).send(myTasks)
    } catch (e) {
        res.status(500).send(e.message)
    }
})

//Code To Update Task
router.patch('/tasks/:id', auth, async(req, res) => {
    const _id = req.params.id
    const updates = Object.keys(req.body)
    const allowUpadtes = ['task_Name', 'isCompleted']
    const isValidOperations = updates.every((update) => {
        return allowUpadtes.includes(update)
    })
    if (!isValidOperations) {
        return res.status(400).send("Invalid Update")
    }
    try {
        //const myTasks = await MyTasks.findById(_id)
        const myTasks = await MyTasks.findOne({ _id, owner: req.user._id })
            //const myTasks = await MyTasks.findByIdAndUpdate(_id, req.body, { new: true, runValidators: true })
        if (!myTasks) {
            res.status(404).send("Task Not Found")
        }
        updates.forEach((update) => myTasks[update] = req.body[update])
        await myTasks.save()
        res.status(200).send(myTasks)
    } catch (e) {
        res.status(500).send(e.message)
    }
})

router.delete('/tasks/:id', auth, async(req, res) => {
    const _id = req.params.id
    try {
        const myTasks = await MyTasks.findOneAndDelete({ _id, owner: req.user._id })
            //
        if (!myTasks) {
            res.status(404).send('Task Not Found')
        }
        res.status(200).send(myTasks)
    } catch (e) {
        res.status(500).send(e.message)
    }
})

module.exports = router