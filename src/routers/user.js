const express = require('express')
const User = require('../models/user')
const auth = require('../middleware/auth')
const router = new express.Router();
const multer = require('multer')
const sharp = require('sharp')
const { sendWelcomeEmail, sendCancelEmail } = require('../emails/account')

// router.get('/test', (req, res) => {
//     res.send('I am the only looser.')
// })

router.post('/users', async(req, res) => {
    const user = new User(req.body)
    try {
        await user.save()
        sendWelcomeEmail(user.email, user.name)
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (err) {
        res.status(400).send(err.message)
    }
})

router.post('/users/login', async(req, res) => {
    try {
        const user = await User.findByCredential(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.send({ user, token })
    } catch (e) {
        res.status(400).send(e.message)
    }
})

router.post('/users/logout', auth, async(req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save()
        res.send("Loged Out Successfully.")
    } catch (e) {
        res.status(500).send(e.message)
    }
})

router.post("/users/logoutAll", auth, async(req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send("Logged Out From all The Session.")
    } catch (e) {
        res.status(500).send(e.message)
    }
})

router.get('/users/me', auth, async(req, res) => {
    // try {
    //     const user = await User.find({})
    //     res.status(200).send(user)
    // } catch (e) {
    //     res.status(500).send(e.message)
    // }
    res.send(req.user)
})

// router.get('/users/:id', async(req, res) => {
//     const _id = req.params.id
//     try {
//         const user = await User.findById(_id)
//         if (!user) {
//             return res.status(404).send("User Not Found")
//         }
//         res.status(200).send(user)
//     } catch (e) {
//         res.status(500).send(e.message)
//     }
// })

router.patch('/users/me', auth, async(req, res) => {
    const _id = req.params.id
    const updates = Object.keys(req.body)
    const allowUpadtes = ['name', 'age', 'phone', 'email', 'password']
    const isValidOperation = updates.every((update) => allowUpadtes.includes(update))
    if (!isValidOperation) {
        return res.status(400).send({ "error": "invalid update" })
    }
    try {
        // const user = await User.findById(_id)
        updates.forEach((update) => req.user[update] = req.body[update])
        await req.user.save()
            //const user = await User.findByIdAndUpdate(_id, req.body, { new: true, runValidators: true })
            // if (!user) {
            //     return res.status(404).send("User not Found")
            // }
        res.status(200).send(req.user)
    } catch (e) {
        res.status(400).send(e.message)
    }
})

router.delete('/users/me', auth, async(req, res) => {
    const _id = req.params.id
    try {
        // const user = await User.findByIdAndDelete(req.user._id)
        // if (!user) {
        //     res.status(404).send("User not Found.")
        // }
        await req.user.remove()
        sendCancelEmail(req.user.email, req.user.name)
        res.status(200).send(req.user)
    } catch (e) {
        res.status(500).send(e.message)
    }
})

const upload = multer({
    //dest: 'avatars',
    limits: {
        fileSize: 1000000,
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            cb(new Error("Please Upload a Photo."))
        }
        cb(undefined, true)
    }
})
router.post('/users/me/avatar', auth, upload.single('avatar'), async(req, res) => {
    const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()
    req.user.avatar = buffer
    await req.user.save()
    res.send('Profile Pic Uploaded Successfully')
}, (err, req, res, next) => {
    res.status(400).send({ error: err.message })
})

router.delete('/users/me/avatar', auth, async(req, res) => {
    req.user.avatar = undefined
    await req.user.save()
    res.status(200).send("Profile pic deleted")
}, (err, req, res, next) => {
    res.status(400).send({ error: err.message })
})

router.get('/users/:id/avatar', async(req, res) => {
    try {
        const user = await User.findById(req.params.id)

        if (!user || !user.avatar) {
            throw new Error("Cannot Find User or User Image")
        }

        res.set('Content-Type', 'image/png')
        res.send(user.avatar)
    } catch (e) {
        res.status(404).send("Could not Find The Image.")
    }
})

module.exports = router