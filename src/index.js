const express = require('express')
require('./db/mongoose')
const User = require('./models/user')
const MyTasks = require('./models/task')
const userRouter = require('./routers/user')
const taskRouter = require('./routers/task')

const app = express()
const port = process.env.PORT

// const multer = require('multer')
// const upload = multer({
//     dest: 'images',
//     limits: {
//         fileSize: 1000000
//     },
//     fileFilter(req, file, cb) {
//         if (!file.originalname.match(/\.(doc|docx)$/)) {
//             return cb(new Error("Please upload a word document."))
//         }
//         cb(undefined, true)
//     }
// })
// const errMiddleWare = (req, res, next) => {
//         throw new Error("Middleware Error.")
//     }
//     //upload.single('upload')
// app.post('/upload', upload.single('upload'), (req, res) => {
//     res.send("ok")
// }, (err, req, res, next) => {
//     res.status(400).send({ error: err.message })
// })

// app.use((req, res, next) => {
//     // if (req.method === 'GET') {
//     //     res.send('GET Requests are Disabled.')
//     // } else {
//     //     next()
//     // }
//     res.status(503).send('Site Is Under Maintainance.')
// })

app.use(express.json())
app.use(userRouter)
app.use(taskRouter)

// app.post('/users', async(req, res) => {
//     const user = new User(req.body)
//     try {
//         await user.save()
//         res.status(201).send(user)
//     } catch (err) {
//         res.status(400).send(err.message)
//     }
// })

// app.get('/users/:name', async(req, res) => {
//     const name = req.params.name;
//     try {
//         const user = await User.findOne({ name })
//         if (!user) {
//             return res.status(404).send("User not Found")
//         }
//         res.status(200).send(user)
//     } catch (e) {
//         res.status(500).send(e.message)
//     }
// })

app.listen(port, () => {
    console.log("Server is running on port " + port)
})


// const bcrypt = require('bcrypt')

// const myFunction = async() => {
//     const pass1 = "RedRed@12345"
//     const hashPass = await bcrypt.hash(pass1, 8)
//     console.log(pass1)
//     console.log(hashPass)
//     const isMatch = await bcrypt.compare("RedRed@12345", hashPass)
//     console.log(isMatch)
// }
// myFunction()

// const jwt = require('jsonwebtoken')
// const myFunction2 = async() => {
//     const token = jwt.sign({ _id: 'abc123' }, 'iamchiranjeeb', { expiresIn: '1 hour' })
//     console.log(token)
//     const data = jwt.verify(token, 'iamchiranjeeb')
//     console.log(data)
// }
// myFunction2()

// const test = async() => {
//     // const task = await MyTasks.findById('5fb3d6bc1e3d942453e62752')
//     // await task.populate('owner').execPopulate()
//     // console.log(task)

//     const user = await User.findById('5fb3ca0f570b5f1a2331c09d')
//     await user.populate('userTask').execPopulate()
//     console.log(user.userTask)
// }
// test()