const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)


// sgMail.send({
//     to: 'chandansahoo438@gmail.com',
//     from: 'chandansahoo438@gmail.com',
//     subject: 'Hey',
//     text: "What happened"
// }) 

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'chandansahoo438@gmail.com',
        subject: 'Task App',
        text: "Welcome to Task App " + name + "."
    })
}

const sendCancelEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'chandansahoo438@gmail.com',
        subject: "Deleting Your Task App",
        text: name + " deleting your account."
    })
}

module.exports = {
    sendWelcomeEmail,
    sendCancelEmail
}