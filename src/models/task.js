const mongoose = require('mongoose')
const validator = require('validator')


const taskSchema = new mongoose.Schema({
    task_Name: {
        type: String,
        required: true,
        trim: true,
    },
    isCompleted: {
        type: Boolean,
        default: false,
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User"
    }
}, {
    timestamps: true
})

const MyTasks = mongoose.model('my_tasks', taskSchema)

module.exports = MyTasks