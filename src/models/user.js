const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const MyTasks = require('./task')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
    },
    age: {
        type: Number,
        trim: true,
        min: 18,
        max: 30,
        validate(val) {
            if (val < 0) {
                throw new Error("Age cant be a -ve number")
            }
        }
    },
    phone: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        validate(val) {
            if (val.length < 10 || val.length > 10) {
                throw new Error("Invalid phone Number")
            }
        }
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase: true,
        validate(val) {
            if (!validator.isEmail(val)) {
                throw new Error("This is not a valid mail.")
            }
        }
    },
    password: {
        type: String,
        required: true,
        trim: true,
        validate(val) {
            let lowerCase = /[a-z]/g;
            let upperCase = /[A-Z]/g;
            let specialCharacter = /[!@#$%?/\|:;~`^&*]/;
            let number = /[0-9]/g;
            if (val.length < 8) {
                throw new Error("Password Length Should be greater than or equal to 8.")
            } else if (!val.match(lowerCase)) {
                throw new Error("Password Should Include a Lower Case.")
            } else if (!val.match(upperCase)) {
                throw new Error("Password should Include an Upper Case.")
            } else if (!val.match(number)) {
                throw new Error("Password should include a Number.")
            } else if (!val.match(specialCharacter)) {
                throw new Error("Password should include a special Character")
            } else {
                return true;
            }
        }
    },
    tokens: [{
        token: {
            type: String,
            required: true,
        }
    }],
    avatar: {
        type: Buffer
    }
}, {
    timestamps: true
})

userSchema.virtual('userTask', {
    ref: "my_tasks",
    localField: '_id',
    foreignField: 'owner'
})

userSchema.methods.toJSON = function() {
    const user = this
    const userObject = user.toObject()

    delete userObject.password
    delete userObject.tokens
    delete userObject.avatar

    return userObject
}

userSchema.methods.generateAuthToken = async function() {
    const user = this
    const token = jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET)
    user.tokens = user.tokens.concat({ token })
    await user.save()
    return token
}

userSchema.statics.findByCredential = async(email, password) => {
    const user = await User.findOne({ email })
    if (!user) {
        throw new Error("Bad Credential !!")
    }
    const isMatchPass = await bcrypt.compare(password, user.password)
    if (!isMatchPass) {
        throw new Error("Bad Credential !!")
    }

    return user
}


// Hash The Plane Text Password Before Saving
userSchema.pre('save', async function(next) {
    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }
    console.log("Before Saving")
    next()
})

// Delete User Task When User is Removed
userSchema.pre('remove', async function(next) {
    const user = this
    await MyTasks.deleteMany({ owner: user._id })
    next()
})
const User = mongoose.model('User', userSchema)

module.exports = User